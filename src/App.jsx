import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'

import Navbar from './components/Navbar/Navbar'
import TheBand from './components/TheBand/TheBand'
import TourDates from './components/TourDates/TourDates'
import Contact from './components/Contact/Contact'
import Carousel from './components/Carousel/Carousel'
import Footer from './components/Footer/Footer'

function App() {
  let imgC=["citta1.jpg","citta2.jpg","citta3.jpg"];
  let nameC=["Citta 1", "Citta 2", "Citta 3"];

  return (
    <>
      <Navbar/>
      <Carousel namesCity={nameC} imgsCity={imgC}/>
      <TheBand/>
      <TourDates/>
      <Contact/>
      <Footer/>
    </>
  )
}

export default App
