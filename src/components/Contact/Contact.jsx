import "./Contact.css"

export default function Contact(){
    return(
        <div id="contact">
            <div className="text-center">
                <br/>
                <h5 className="my-4 spacing">Contact</h5>
                <p className="fst-italic fw-light my-2">We love our fans!</p>
            </div>

            <div className="container mw60">
                <div className="row">
                    <div className="col-sm-4">
                        <p>Fan? Drop a note.</p>
                        <p>Chicago, US</p>
                        <p>Phone: +00 1515151515</p>
                        <p>Email: mail@mail.com</p>
                    </div>
                    <form className="col-sm-8">
                        <div className="row">
                            <div className="col-sm-6 form-floating mb-3">
                                <input type="text" className="form-control" id="input1" placeholder="name"/>
                                <label for="input1" className="form-label text-secondary">Name</label>
                            </div>
                            <div className="col-sm-6 form-floating mb-3">
                                <input type="email" className="form-control" id="input2" placeholder="name@example.com"/>
                                <label for="input2" className="text-secondary">Email</label>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12 form-floating">
                                    <textarea className="form-control ta" placeholder="Leave a comment here" id="input3" resize:none></textarea>
                                    <label for="input3" className="text-secondary">Comments</label>
                            </div>
                        </div>
                        <div className="row my-4">
                            <div className="col-sm-10"></div>
                            <div className="col-sm-2">
                                <button className="btn btn-dark" type="submit">Send</button>
                            </div>
                        </div>
                    </form>
                    
                </div>
                
            </div>
        </div>
    )
}