export default function Navbar(){
    return(
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top opacity-75">
            <div className="container-fluid">
                <a className="navbar-brand" href="#">Logo</a>

                <ul className="nav navbar-nav justify-content-end text-uppercase active mx-3">
                    <li className="nav-item">
                        <a className="nav-link" aria-current="page" href="#">home</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#band">band</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#tour">tour</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#contact">contact</a>
                    </li>
                    <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle " role="button" data-bs-toggle="dropdown" aria-expanded="false">menu</a>
                        <ul className="dropdown-menu dropdown-menu-dark ">
                            <li><a className="dropdown-item" href="#">Item 1</a></li>
                            <li><a className="dropdown-item" href="#">Item 2</a></li>
                            <li>
                                <hr className="dropdown-divider"/>
                            </li>
                            <li><a className="dropdown-item" href="#">Item 3</a></li>
                        </ul>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">
                            <svg width="20" height="20" className="DocSearch-Search-Icon" viewBox="0 0 20 20"><path d="M14.386 14.386l4.0877 4.0877-4.0877-4.0877c-2.9418 2.9419-7.7115 2.9419-10.6533 0-2.9419-2.9418-2.9419-7.7115 0-10.6533 2.9418-2.9419 7.7115-2.9419 10.6533 0 2.9419 2.9418 2.9419 7.7115 0 10.6533z" stroke="currentColor" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round"></path></svg>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    )
}