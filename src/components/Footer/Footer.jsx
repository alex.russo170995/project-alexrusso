export default function Footer(){
    return(
        <footer className="bg-dark">
            <div className="text-light text-center my-4 h30">
                <br/>
                <h5 className="b">Test Footer</h5>
                <p>Descrizione...</p>
            </div>
            <hr className="text-light"/>
            <div className="text-light text-center">
                <small>Copyright &copy;2017 Test.</small>
            </div>
        </footer>
    )
}