import "./TheBand.css"

export default function(){
    return(
        <div className="text-center" id="band">
                <br/>
                <h5 className="text-uppercase my-4">the band</h5>
                <p className="fst-italic fw-light my-1">We love music!</p>
                <p className="my-1">Descrizione.....</p>

                <div className="d-flex justify-content-center my-4">
                    <div className="p2 card border-0 mx-4 w18">
                        <div className="card-body">
                            <h5 className="card-title">Città 1</h5>
                        </div>
                        <img src="citta1.jpg" className="card-img-btn rounded-circle" alt="immagine città 1"/>
                    </div>
                    <div className="p2 card border-0 mx-4 w18">
                        <div className="card-body">
                            <h5 className="card-title">Città 2</h5>
                        </div>
                        <img src="citta2.jpg" className="card-img-btn rounded-circle" alt="immagine città 2"/>
                    </div>
                    <div className="p2 card border-0 mx-4 w18">
                        <div className="card-body">
                            <h5 className="card-title">Città 3</h5>
                        </div>
                        <img src="citta3.jpg" className="card-img-btn rounded-circle" alt="immagine città 3"/>
                    </div>
                </div>
            </div>
    )
}