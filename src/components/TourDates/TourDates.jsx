import "./TourDates.css"

export default function TourDates(){
    return(
        <div className="bg-dark" id="tour">
                <div className="text-center">
                    <br/>
                    <h5 className="text-uppercase text-light my-4 spacing">tour dates</h5>
                    <p className="fst-italic fw-light text-light my-1">Lorem...music.</p>
                    <p className="fst-italic fw-light text-light my-1">Remember...ticket!</p>
                </div>
                

                <div className="container my-2 mw60">
                    <div className="row">
                        <div className="border border-black-50 bg-light h3">
                            <span className="lh26">September </span>
                            <span className="text-light bg-danger rounded lh26">Soul Out!</span>
                        </div>
                    </div>
                    <div className="row">
                        <div className="border border-black-50 bg-light h3">
                            <span className="lh26">October </span>
                            <span className="text-light bg-danger rounded lh26">Soul Out!</span>
                        </div>
                    </div>
                    <div className="row">
                        <span className="lh26 border border-black-50 bg-light h3">November</span>
                    </div>
                    <div className="row">

                        {/* promemoria, si può sistemare con "col" oppure spostando " d-flex justify-content-center" in un <div> interno
                        per ridimenzionare correttamente, ma si perde l'effetto di ridimenzionamento ottenuto in html */}
                        <div className=" d-flex justify-content-center">
                            <div className="card my-3 w18">
                                <img src="citta1.jpg" className="card-img-top" alt="immagine città 1"/>
                                <div className="card-body">
                                    <h5 className="card-title">Città 1</h5>
                                    <p className="card-text">Descrizione...</p>
                                    <a href="#" className="btn btn-dark">Buy Ticket</a>
                                </div>
                            </div>

                            <div className="card m-3 w18">
                                <img src="citta2.jpg" className="card-img-top" alt="immagine città 2"/>
                                <div className="card-body">
                                    <h5 className="card-title">Città 2</h5>
                                    <p className="card-text">Descrizione...</p>
                                    <a href="#" className="btn btn-dark">Buy Ticket</a>
                                </div>
                            </div>

                            <div className="card my-3 w18">
                                <img src="citta3.jpg" className="card-img-top" alt="immagine città 3"/>
                                <div className="card-body">
                                    <h5 className="card-title">Città 3</h5>
                                    <p className="card-text">Descrizione...</p>
                                    <a href="#" className="btn btn-dark">Buy Ticket</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
    )
}