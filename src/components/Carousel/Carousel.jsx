// import Immagine from "../../assets/Immagine.png"
// <src={Immagine}/>

export default function Carousel({namesCity,imgsCity}){
    let citys=[namesCity.length];
    let buttons=[namesCity.length];

    //inserisco elementi nell'array per ogni città da inserire nel carosello
    for(let i=0;i<namesCity.length;i++){
        if(i==0){
            buttons.push(<button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"/>);
        }else{
            buttons.push(<button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to={i} aria-label={"Slide "+(i+1)}/>);
        }

        citys.push(
            <div className={i==0?"carousel-item active":"carousel-item"}>
                <img src={imgsCity[i]} className="d-block w-100" alt={"immagine "+namesCity[i]}/>
                <div className="carousel-caption d-none d-md-block">
                    <h5>{namesCity[i]}</h5>
                    <p>Descrizione ...</p>
                </div>
            </div>
        );
    }
    return(
        <div id="carouselExampleCaptions" className="carousel slide">
            <div className="carousel-indicators">
                {buttons.map(b =>{return(b)})}
            </div>
            <div className="carousel-inner">
                {citys.map(c =>{return(c)})}
            </div>
            <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Previous</span>
            </button>
            <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Next</span>
            </button>
        </div>
    )
}